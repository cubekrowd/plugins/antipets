package net.cubekrowd.antipets;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagConflictException;
import java.util.Stack;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import simplepets.brainsynder.api.entity.IEntityPet;
import simplepets.brainsynder.api.event.entity.PetEntitySpawnEvent;
import simplepets.brainsynder.api.event.entity.PetMountEvent;
import simplepets.brainsynder.api.event.entity.PrePetHatEvent;
import simplepets.brainsynder.api.pet.PetType;
import simplepets.brainsynder.api.plugin.SimplePets;

public class AntiPetsPlugin extends JavaPlugin implements Listener {
    public StateFlag petsFlag;
    public StateFlag petRideFlag;
    public StateFlag petHatFlag;
    public StateFlag petCollisionsFlag;
    public boolean registerFailed;

    public StateFlag registerStateFlag(String name, boolean defaultValue) {
        var flagRegistry = WorldGuard.getInstance().getFlagRegistry();
        var res = new StateFlag(name, defaultValue);
        try {
            flagRegistry.register(res);
        } catch (FlagConflictException | IllegalStateException e) {
            var existingFlag = flagRegistry.get(name);
            if (existingFlag instanceof StateFlag) {
                res = (StateFlag) existingFlag;
            } else {
                getLogger().severe("Flag '" + name + "' already registered by someone else");
                registerFailed = true;
            }
        }
        return res;
    }

    @Override
    public void onLoad() {
        registerFailed = false;
        petsFlag = registerStateFlag("pets", true);
        petRideFlag = registerStateFlag("pet-ride", true);
        petHatFlag = registerStateFlag("pet-hat", true);
        petCollisionsFlag = registerStateFlag("pet-collisions", true);
    }

    @Override
    public void onEnable() {
        if (registerFailed) {
            // too bad
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        petsFlag = null;
        petRideFlag = null;
        petHatFlag = null;
        petCollisionsFlag = null;
    }

    public boolean hasNestedPassenger(Entity entity, Entity match) {
        var stack = new Stack<Entity>();
        stack.push(entity);
        while (!stack.isEmpty()) {
            var e = stack.pop();
            if (e == match) {
                return true;
            }
            stack.addAll(e.getPassengers());
        }
        return false;
    }

    public boolean isPetVehicle(IEntityPet pet) {
        // @NOTE(traks) should be good enough. The SimplePets API for pet
        // vehicles sucks. They maintain some private field to track which pet
        // is ridden, but it desyncs faster than the Minecraft memory allocation
        // rate.
        //
        // They also do a bunch of teleports and whatever when you start riding
        // a pet, so it's impossible to figure out whether the owner has started
        // riding their pet or not...
        return hasNestedPassenger(pet.getEntity(), pet.getPetUser().getPlayer());
    }

    public boolean isPetHat(IEntityPet pet) {
        // @NOTE(traks) Issues similar to SimplePets vehicle API, but seems to
        // go out of sync less often.
        return hasNestedPassenger(pet.getPetUser().getPlayer(), pet.getEntity());
    }

    @EventHandler
    public void onServerTick(ServerTickEndEvent e) {
        for (var p : Bukkit.getOnlinePlayers()) {
            if (!SimplePets.getUserManager().isUserCached(p)) {
                // @NOTE(traks) If not cached, player joined for the first time
                // since SimplePets got enabled and SimplePets didn't create a
                // user object for them.
                //
                // Without this check, we will force SimplePets #75 to create a
                // user object, which will result in the stored 'spawned pets'
                // being spawned from local cache. Then a few ticks later,
                // SimplePets will load the data again from the database in its
                // join listener.
                //
                // This results in the pets spawned a second time, but their
                // UUIDs will be the same, which results in spawning failing and
                // the first spawned pets glitching out (no data, not moving).
                // And SimplePets will think the user has no pets out.
                continue;
            }

            var petOwner = SimplePets.getUserManager().getPetUser(p).get();

            var localPlayer = WorldGuardPlugin.inst().wrapPlayer(p);
            var canBypass = WorldGuard.getInstance().getPlatform().getSessionManager()
                    .hasBypass(localPlayer, localPlayer.getWorld());
            if (canBypass) {
                continue;
            }

            for (var pet : petOwner.getPetEntities()) {
                var query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();
                var petLoc = pet.getEntity().getLocation();
                var petLocWE = new Location(localPlayer.getExtent(), petLoc.getX(), petLoc.getY(), petLoc.getZ());

                if (!query.testState(petLocWE, localPlayer, petsFlag)) {
                    petOwner.removePet(pet.getPetType());
                    var msg = new ComponentBuilder("You can't use pets here!").color(ChatColor.RED).create();
                    p.sendMessage(msg);
                    continue;
                } else if (pet.getPetType() == PetType.SHULKER) {
                    // Players can stand on shulkers, force themselves into lay-down
                    // mode, and possibly perform other shenanigans. Don't allow these
                    // kinds of things in marked regions

                    if (!query.testState(petLocWE, localPlayer, petCollisionsFlag)) {
                        petOwner.removePet(pet.getPetType());
                        var msg = new ComponentBuilder("You can't use shulker pets here!").color(ChatColor.RED).create();
                        p.sendMessage(msg);
                        continue;
                    }
                }

                if (isPetVehicle(pet) && !query.testState(petLocWE, localPlayer, petRideFlag)) {
                    petOwner.setPetVehicle(pet.getPetType(), false);
                    // @NOTE(traks) make sure the player dismounts in case
                    // SimplePets does something weird internally (not out of
                    // the question...)
                    p.leaveVehicle();
                    var msg = new ComponentBuilder("You can't ride pets here!").color(ChatColor.RED).create();
                    p.sendMessage(msg);
                    continue;
                }

                if (isPetHat(pet) && !query.testState(localPlayer.getLocation(), localPlayer, petHatFlag)) {
                    petOwner.setPetHat(pet.getPetType(), false);
                    var msg = new ComponentBuilder("You can't use pet hats here!").color(ChatColor.RED).create();
                    p.sendMessage(msg);
                    continue;
                }
            }
        }
    }

    @EventHandler
    public void onPetMount(PetMountEvent e) {
        var pet = e.getEntity();
        var p = pet.getPetUser().getPlayer();
        if (p == null) {
            return;
        }

        var localPlayer = WorldGuardPlugin.inst().wrapPlayer(p);
        var canBypass = WorldGuard.getInstance().getPlatform().getSessionManager()
                .hasBypass(localPlayer, localPlayer.getWorld());
        if (canBypass) {
            return;
        }

        var query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();
        var petLoc = pet.getEntity().getLocation();
        var petLocWE = new Location(localPlayer.getExtent(), petLoc.getX(), petLoc.getY(), petLoc.getZ());

        // @NOTE(traks) pet gets teleported to the player before riding, so test
        // the player's location instead of the pet location
        if (!query.testState(petLocWE, localPlayer, petsFlag)) {
            e.setCancelled(true);
            var msg = new ComponentBuilder("You can't use pets here!").color(ChatColor.RED).create();
            p.sendMessage(msg);
        } else if (!query.testState(localPlayer.getLocation(), localPlayer, petRideFlag)) {
            e.setCancelled(true);
            var msg = new ComponentBuilder("You can't ride pets here!").color(ChatColor.RED).create();
            p.sendMessage(msg);
        }
    }

    @EventHandler
    public void onPetHat(PrePetHatEvent e) {
        var pet = e.getEntityPet();
        var p = pet.getPetUser().getPlayer();
        if (p == null) {
            return;
        }

        if (e.getEventType() == PrePetHatEvent.Type.SET) {
            var localPlayer = WorldGuardPlugin.inst().wrapPlayer(p);
            var canBypass = WorldGuard.getInstance().getPlatform().getSessionManager()
                    .hasBypass(localPlayer, localPlayer.getWorld());
            if (canBypass) {
                return;
            }

            var query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();

            if (!query.testState(localPlayer.getLocation(), localPlayer, petsFlag)) {
                e.setCancelled(true);
                var msg = new ComponentBuilder("You can't use pets here!").color(ChatColor.RED).create();
                p.sendMessage(msg);
            } else if (!query.testState(localPlayer.getLocation(), localPlayer, petHatFlag)) {
                e.setCancelled(true);
                var msg = new ComponentBuilder("You can't use pet hats here!").color(ChatColor.RED).create();
                p.sendMessage(msg);
            }
        }
    }

    @EventHandler
    public void onPetSpawn(PetEntitySpawnEvent e) {
        var p = e.getUser().getPlayer();
        if (p == null) {
            return;
        }

        var localPlayer = WorldGuardPlugin.inst().wrapPlayer(p);
        var canBypass = WorldGuard.getInstance().getPlatform().getSessionManager()
                .hasBypass(localPlayer, localPlayer.getWorld());
        if (canBypass) {
            return;
        }

        var query = WorldGuard.getInstance().getPlatform().getRegionContainer().createQuery();
        var petLoc = e.getEntity().getEntity().getLocation();
        var petLocWE = new Location(localPlayer.getExtent(), petLoc.getX(), petLoc.getY(), petLoc.getZ());

        // @NOTE(traks) SimplePets sends a message to the player when spawning
        // fails, so feels a bit double if we would send an additional message
        // ourselves
        if (!query.testState(petLocWE, localPlayer, petsFlag)) {
            e.setCancelled(true);
        } else if (e.getEntity().getPetType() == PetType.SHULKER) {
            if (!query.testState(petLocWE, localPlayer, petCollisionsFlag)) {
                e.setCancelled(true);
            }
        }
    }
}
