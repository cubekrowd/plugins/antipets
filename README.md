# AntiPets

This plugin adds flags to WorldGuard to configure to what extent pets can be used in a region. The flags are as follows:

| Flag                          | Description
|---                            |---
| pets                          | If false, pets can't enter the region and can't be used as hats in the region. True by default.
| pet&#x2011;ride               | If false, pets can't be ridden inside the region. True by default.
| pet&#x2011;hat                | If false, pets can't be used as hats inside the region. True by default.
| pet&#x2011;collisions         | If false, prevents the use of shulker pets inside the region. True by default.
